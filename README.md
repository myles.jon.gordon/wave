# wave

Music library management/playback program with support for the main filetypes

- [ ] Implement a library system detecting artist, album and song
    - [ ] Implement tagging via MusicBrainz
- [ ] Add support for FLAC, MP3, WAV and any others